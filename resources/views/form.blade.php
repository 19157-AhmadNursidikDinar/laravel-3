@extends('layouts.master')
@section('judul')
Buat Account Baru
@endsection
@section('content')
<h3>Sign Up Form</h3>
<form action="/welcome" method="post">
@csrf
    <label>first name :</label><br><br>
    <input type="text" name="depan"><br><br>
    <label>Last name :</label><br><br>
    <input type="text" name="belakang"><br><br>
    <label>Gender</label><br><br>
    <input type="radio" id="male" name="gender" value="Male">
    <label for="male">Male</label><br>
    <input type="radio" id="female" name="gender" value="Female">
    <label for="female">Female</label><br><br>
    <label>Nationality</label><br><br>
    <select name="nationality">
        <option value="ind">Indonesia</option>
        <option value="usa">Amerika Serikat</option>
    </select><br><br>
    <label>Language Spoken</label><br><br>
    <input type="checkbox">Bahasa Indonesia <br>
    <input type="checkbox">English <br>
    <input type="checkbox">Other <br><br>
    <label>Bio</label> <br><br>
    <textarea name="bio" cols="30" rows="10"></textarea><br>
    <button type="submit">Sign Up</button>
</form>
@endsection